<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert</title>
</head>
<body>
		<c:if test = "${flag}">

				<form action="${pageContext.request.contextPath}/insert" method = "POST">
				      
						<table>
							<tr>
								<td><label>ID</label></td>
								<td><input name="id_condominio" /> </td>
				
							</tr>
							<tr>
								<td><label>Descrizione</label></td>
								<td><input name="descrizione" /> </td>
				
							</tr>
							<tr>
								<td><label>Indirizzo</label></td>
								<td><input name="indirizzo" /> </td>
				
							</tr>
							<tr>
								<td><label>Citta</label></td>
								<td><input name="citta" /> </td>
				
							</tr>
							
							<tr>
								<td><label >Provincia</label></td>
								<td><input name="provincia" /> </td>
							</tr>
							<tr>
								<td><label>Regione</label></td>
								<td><input name="regione" /> </td>
				
							</tr>
							<tr>
								<td><label>ID Amministratore</label></td>
								<td><input name="id_amministratore" /> </td>
				
							</tr>
							<tr>
								<td colspan="2"> <input type="submit" value ="Inserisci"/></td>
							</tr>
						</table>
					</form>
					
				</c:if>
				
				<c:if test = "${!flag}">

				<form action="${pageContext.request.contextPath}/update" method = "POST">
				      
						<table>
							<tr>
								<td><label>Descrizione</label></td>
								<td><input name="descrizione" /> </td>
				
							</tr>
							<tr>
								<td><label>Indirizzo</label></td>
								<td><input name="indirizzo" /> </td>
				
							</tr>
							<tr>
								<td><label>Citta</label></td>
								<td><input name="citta" /> </td>
				
							</tr>
							
							<tr>
								<td><label >Provincia</label></td>
								<td><input name="provincia" /> </td>
							</tr>
							<tr>
								<td><label>Regione</label></td>
								<td><input name="regione" /> </td>
				
							</tr>
							<tr>
								<td><label>ID Amministratore</label></td>
								<td><input name="id_amministratore" /> </td>
				
							</tr>
							<tr>
								<td> <input type="submit" value ="Aggiorna"/></td>
								<td>
									<input type="button" value="Back" onClick="javascript:history.back()" name ="button" />
								</td>
							</tr>
						</table>
					</form>
				</c:if>
</body>
</html>