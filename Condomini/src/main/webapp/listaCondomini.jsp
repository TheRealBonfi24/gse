<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored = "false" %>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Condomini</title>
	</head>
	<body>
		<h2>Lista Condomini</h2>
		<form>
		<table border = "2">
			<tr>
				<th>check</th>
				<th>id_condominio</th>
				<th>descrizione</th>
				<th>indirizzo</th>
				<th>citta</th>
				<th>provincia</th>
				<th>id_amministratore</th>
	
			</tr>
			 <c:forEach items ="${listaCondomini}" var="test"> 
			<tr>
				<td><input type="radio" name="check" value="${test.id_condominio}"> </td>
				<td><c:out value = "${test.id_condominio}"/></td>
	        	<td><c:out value = "${test.descrizione}"/></td>
	        	<td><c:out value = "${test.indirizzo}"/></td>
	        	<td><c:out value = "${test.citta}"/></td>
	        	<td><c:out value = "${test.provincia}"/></td>
	        	<td><a href="printadmin/${test.id_amministratore}"><c:out value = "${test.id_amministratore}"/></a></td>
	        </tr>
	      </c:forEach>
	     </table>
	
			<table>
				<tr>
					<td>
						<input type="submit" value="Inserisci" formaction="inserting" />
					</td>
					<td>
						<input type="submit" value="Aggiorna" formaction="updating" />
					</td>
					<td>
						<input type="submit" value="Cancella" formaction="delete" />
					</td>
					<td>
						<input type="button" value="Back" onClick="javascript:history.back()" name ="button" />
					</td>
				</tr>
			</table>
		</form> 
	</body>
</html>