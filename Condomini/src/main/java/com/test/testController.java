package com.test;

import java.io.Reader;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.test.pojo.Condomini;
import com.test.pojo.Utenti;

//@SessionAttributes(value="userid", types={String.class})
@Controller
public class testController {

	
	//prefix + "index" + + suffix
	// /        index       .jsp
	
	final String index="index";	
	boolean flag;
	int id_to_update;
	

	// tutti gli API sono di DEFAULT
	@RequestMapping(value = "/testgethttp" )
	public String testGetHttp(HttpServletRequest req, HttpServletResponse res, HttpSession hSession, Writer out, Reader in) {
		
		String nome=req.getParameter("nome");
		System.out.println("nome: "+nome);
		
		return index;
	}
	
	
	/*
	@RequestMapping("/testDB")
	public String testDB() {
		DB db = new DB();
		return index;
	}*/
	
	@RequestMapping("/readDB")
	public String readDB() throws SQLException {
		DB db = new DB();
		List<Condomini> listcon = db.printDate();
		for(Condomini c : listcon)
			System.out.println(c.getDescrizione() + " " + c.getRegione());
		return index;
	}
	
	@RequestMapping(value = "/homepage", method = RequestMethod.GET)
	public String loginPage(){
		
		return "loginPage";
	}
	
	@RequestMapping(value = "/homepage", method = RequestMethod.POST)
	public String loginPage(Map <String,Object> map,@RequestParam("email") String email, @RequestParam("pwd") String password){
		
		DB db = new DB();
		List<Utenti> listcon= null;
		try {
			listcon = db.readUtenti();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(Utenti c : listcon) {
			if(email.equals(c.getEmail()) && password.equals(c.getPassword()))
				return "homepage";
			else {
				boolean ERROR = true;
				map.put("flag", ERROR);
				return "index";	
			}
				
		}
			
		return "homepage";
	}
	
	
	@RequestMapping("/condomini")
	public ModelAndView condomini() throws SQLException {
		ModelAndView mView = new ModelAndView("listaCondomini");
		List<Condomini> list = new ArrayList<Condomini>();
		DB db = new DB();
		list=db.printDate();
		mView.addObject("listaCondomini",list);
		return mView;
	}
	
	
	@RequestMapping(value = "/inserting", method = RequestMethod.GET)
	public String inserting(Map <String,Object> map) {
		flag=true;
		map.put("flag", flag);
		return "inserting";
	}
	
	@RequestMapping("/insert")
	public ModelAndView condomini_insert(@RequestParam("id_condominio") int id, @RequestParam("descrizione") String descrizione, @RequestParam("indirizzo") String indirizzo,
			@RequestParam("citta") String citta, @RequestParam("provincia") String provincia, @RequestParam("regione") String regione, @RequestParam("id_amministratore") int id_amministratore) throws SQLException {
		
		ModelAndView mView = new ModelAndView("listaCondomini");
		
		Condomini c = new Condomini (id, descrizione, indirizzo, citta, provincia, regione, id_amministratore);
		
		Condomini.CondominiJDBC jdbc = new Condomini.CondominiJDBC();
		
		//create record into db
		jdbc.create(c);
		
		List<Condomini> list = new ArrayList<Condomini>();
		DB db = new DB();
		list=db.printDate();
		mView.addObject("listaCondomini",list);
		
		return mView;
	}
	
	@RequestMapping(value = "/updating", method = RequestMethod.GET)
	public String updating(Map <String,Object> map, @RequestParam("check") int id) {
		flag=false;
		map.put("flag", flag);
		id_to_update=id;
		return "inserting";
	}
	
	@RequestMapping("/update")
	public ModelAndView condomini_update(@RequestParam("descrizione") String descrizione, @RequestParam("indirizzo") String indirizzo,
			@RequestParam("citta") String citta, @RequestParam("provincia") String provincia, @RequestParam("regione") String regione, @RequestParam("id_amministratore") int id_amministratore) throws SQLException {
		
		ModelAndView mView = new ModelAndView("listaCondomini");
		
		Condomini c = new Condomini (0, descrizione, indirizzo, citta, provincia, regione, id_amministratore);
		
		Condomini.CondominiJDBC jdbc = new Condomini.CondominiJDBC();
		
		//create record into db
		jdbc.update(id_to_update, c);
		
		List<Condomini> list = new ArrayList<Condomini>();
		DB db = new DB();
		list=db.printDate();
		mView.addObject("listaCondomini",list);
		
		return mView;
	}

	@RequestMapping("/printadmin/{id_amministratore}")
	public ModelAndView adminMODEL(@PathVariable("id_amministratore") Integer id) throws SQLException {
		ModelAndView mView = new ModelAndView("printadmin");
		List<Utenti> list = new ArrayList<Utenti>();
		DB db = new DB();
		list=db.admin(id);
		mView.addObject("printadmin",list);
		return mView;
	}
	

	@RequestMapping(value="delete/")
	public ModelAndView condomini_delete(@RequestParam("check") int id) throws SQLException {

	ModelAndView mView = new ModelAndView("listaCondomini");


	Condomini.CondominiJDBC jdbc = new Condomini.CondominiJDBC();

	//create record into db
	jdbc.deleteById(id);

	List<Condomini> list = new ArrayList<Condomini>();
	DB db = new DB();
	list=db.printDate();
	mView.addObject("listaCondomini",list);

	return mView;
	}
	
//	@RequestMapping(value = "delete/{id}", method=RequestMethod.DELETE)
//	public String delete(@PathVariable("id") Integer id) {
//		System.out.println("delete funziona");
//		return "redirect:/homepage2";
//	}
//	
//	@ModelAttribute 
//	public void nomeQualsiasi(@RequestParam(value="id", required=false) String id) {
//		if(id!=null) {
//			System.out.println(id);
//		}
//		System.out.println("modelattribute");
//	}
//	
//	@RequestMapping(value = "homepage2", method = RequestMethod.GET)
//	public String homepage(Map <String,Object> map) {
//		return "homepage";
//	}
	

	
}
