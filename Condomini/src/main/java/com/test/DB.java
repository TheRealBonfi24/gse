package com.test;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.test.pojo.Condomini;
import com.test.pojo.Utenti;

public class DB {
	
	Connection con;
	
	public DB() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/gse?useLegacyDatetimeCode=false"
					+ "&serverTimezone=Europe/Rome&autoReconnect=true&useSSL=false", "root", "root");
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
	}

	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}
	
	public List<Condomini> printDate() throws SQLException{
		
		String sql="select * from condomini";
		DB db = new DB();
		Statement smt=db.getCon().createStatement();
		ResultSet rs = smt.executeQuery(sql);
		
		List<Condomini> list = new ArrayList<Condomini>();

		
		while(rs.next())
		{
			list.add(new Condomini(rs.getInt("id_condominio"), rs.getString("descrizione"), rs.getString("indirizzo"), 
					rs.getString("citta"), rs.getString("provincia"),rs.getString("regione"), rs.getInt("id_amministratore")));
		}
		
		return list;
	}
	
	
	
	public List<Utenti> readUtenti() throws SQLException{
		
		String sql="select nome, cognome_ragsociale, email, pwd from utenti";
		DB db = new DB();
		Statement smt=db.getCon().createStatement();
		ResultSet rs = smt.executeQuery(sql);	
		List<Utenti> list = new ArrayList<Utenti>();
		while(rs.next())
		{
			list.add(new Utenti(rs.getString("email"),rs.getString("pwd")));
		}
		return list;
	}
	
	
	public List<Utenti> admin(int id) throws SQLException{
		
		String sql="select * from utenti where id_utente = " + id;
		DB db = new DB();
		Statement smt=db.getCon().createStatement();
		ResultSet rs = smt.executeQuery(sql);
		
		List<Utenti> list = new ArrayList<Utenti>();

		
		while(rs.next())
		{
			list.add(new Utenti(rs.getInt("id_utente"),rs.getString("cognome_ragsociale")));
		}
		
		return list;
	}


}
