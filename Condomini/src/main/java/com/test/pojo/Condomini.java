package com.test.pojo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Component;

import com.test.DB;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

@Component
public class Condomini {

	private int id_condominio;
	private String descrizione;
	private String indirizzo;
	private String citta;
	private String provincia;
	private String regione;
	private int id_amministratore;
	
	public Condomini() {
		
	}

	
	public Condomini(int id_condominio, String descrizione, String indirizzo, String citta, String provincia,
			String regione, int id_amministratore) {
		this.id_condominio = id_condominio;
		this.descrizione = descrizione;
		this.indirizzo = indirizzo;
		this.citta = citta;
		this.provincia = provincia;
		this.regione = regione;
		this.id_amministratore = id_amministratore;
	}

	public int getId_condominio() {
		return id_condominio;
	}
	public void setId_condominio(int id_condominio) {
		this.id_condominio = id_condominio;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getRegione() {
		return regione;
	}
	public void setRegione(String regione) {
		this.regione = regione;
	}


	public int getId_amministratore() {
		return id_amministratore;
	}


	public void setId_amministratore(int id_amministratore) {
		this.id_amministratore = id_amministratore;
	}


	@Override
	public String toString() {
		return "Condomini [id_condominio=" + id_condominio + ", descrizione=" + descrizione + ", indirizzo=" + indirizzo
				+ ", citta=" + citta + ", provincia=" + provincia + ", regione=" + regione + ", id_amministratore="
				+ id_amministratore + "]";
	}
	
	
	//----------------------------PARTE DB------------------------------
	private static interface CondominiDAO{
		
		//public void setDataSource(DataSource ds);
		
		public void create(int id, String descrizione, String indirizzo, String citta, String provincia, String regione, int id_amministratore) throws SQLException;
		public void create (Condomini c) throws SQLException;
		
		public List<Condomini> getAllC();
		
		public Condomini getById (int id);
		public void deleteById (int id) throws SQLException;
		public void update (int id, Condomini c) throws SQLException;
		
	}
	
	private static class CondominiMapper implements RowMapper<Condomini>{

		
		public Condomini mapRow (ResultSet rs, int rowNum) throws SQLException {

			Condomini c = new Condomini();
			
			c.setId_condominio(rs.getInt("id_condominio"));
			c.setDescrizione(rs.getString("descrizione"));
			c.setIndirizzo(rs.getString("indirizzo"));
			c.setCitta(rs.getString("citta"));
			c.setProvincia(rs.getString("provincia"));
			c.setRegione(rs.getString("regione"));
			c.setId_amministratore(rs.getInt("id_amministratore"));
			
			return c;
			
		}
	}
	
	
	public static class CondominiJDBC implements CondominiDAO{

		private JdbcTemplate jdbc;
		
		
		public void setDataSource(DataSource ds) {

			jdbc = new JdbcTemplate(ds);
			
		}

		
		public void create(int id_condominio, String descrizione, String indirizzo, String citta, String provincia, String regione, int id_amministratore) throws SQLException{


			DB db = new DB();
			PreparedStatement smt= db.getCon().prepareStatement("INSERT INTO condomini "
					+ "(id_condominio, descrizione, indirizzo, citta, provincia, regione, id_amministratore)"
					+ "VALUES (?, ?, ?, ?, ?, ?, ?)");
			smt.setInt(1, id_condominio);
			smt.setString(2, descrizione);
			smt.setString(3, indirizzo);
			smt.setString(4, citta);
			smt.setString(5, provincia);
			smt.setString(6, regione);
			smt.setInt(7, id_amministratore);
			
			smt.executeUpdate();
			
			System.out.println("Inserting into DB new Condominio");
			
		}

		
		public void create(Condomini c) throws SQLException {
			
			create(c.getId_condominio(),
					c.getDescrizione(),
					c.getIndirizzo(),
					c.getCitta(),
					c.getProvincia(),
					c.getRegione(),
					c.getId_amministratore());
			
		}


		public List<Condomini> getAllC() {

			String sql = "SELECT *"
					+ "FROM condomini";
			List<Condomini> bfs = jdbc.query(sql, new CondominiMapper());
			return bfs;
		}

		public Condomini getById(int id) {

			String sql = "Select *"
					+ " from condomini"
					+ " where id_condominio="+id;
			Condomini bf = jdbc.queryForObject(sql, new CondominiMapper());		
			return bf;
		}


		public void deleteById(int id) throws SQLException {
			DB db = new DB();
			PreparedStatement smt= db.getCon().prepareStatement("Delete"
			+ " from condomini"
			+ " where id_condominio="+id);

			smt.execute();

			System.out.println("Deleting from DB Condominio with ID = "+id);

			}


		public void update(int id, Condomini bf) throws SQLException {

			
		
			
			DB db = new DB();
			PreparedStatement smt= db.getCon().prepareStatement("UPDATE condomini"
					+ " SET descrizione=?, indirizzo=?, citta=?, provincia=?, regione=?, id_amministratore=? where id_condominio=?");

			smt.setString(1, bf.getDescrizione());
			smt.setString(2, bf.getIndirizzo());
			smt.setString(3, bf.getCitta());
			smt.setString(4, bf.getProvincia());
			smt.setString(5, bf.getRegione());
			smt.setInt(6, bf.getId_amministratore());
			smt.setInt(7, id);
			
			smt.executeUpdate();
			
			System.out.println("Updating into DB new Condominio");
			
		}
		
		
		
	}	


}
