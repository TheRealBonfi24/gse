package com.test.pojo;

public class Utenti {

	private int id_utente;
	private String nome;
	private String cognome_ragsociale;
	private String email;
	private String password;
	
	
	public Utenti() {}
	
	public Utenti(String email, String password) {
		this.email = email;
		this.password = password;
	}
	public Utenti(int id_utente, String cognome_ragsociale) {
		this.id_utente=id_utente;
		this.cognome_ragsociale = cognome_ragsociale;
	}

	public Utenti(String nome, String cognome_ragsociale, String email, String password) {
		super();
		this.nome = nome;
		this.cognome_ragsociale = cognome_ragsociale;
		this.email = email;
		this.password = password;
	}

	
	public int getId_utente() {
		return id_utente;
	}

	public void setId_utente(int id_utente) {
		this.id_utente = id_utente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome_ragsociale() {
		return cognome_ragsociale;
	}

	public void setCognome_ragsociale(String cognome_ragsociale) {
		this.cognome_ragsociale = cognome_ragsociale;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Utenti [nome=" + nome + ", cognome_ragsociale=" + cognome_ragsociale + ", email=" + email
				+ ", password=" + password + "]";
	}


	
	
	
	
	
	
	
	
}
